        <div class="breadcrumbs">
        	<div class="breadcrumbs-inner">
        		<div class="row m-0">
        			<div class="col-sm-4">
        				<div class="page-header float-left">
        					<div class="page-title">
        						<h1>Berita</h1>
        					</div>
        				</div>
        			</div>
        			<div class="col-sm-8">
        				<div class="page-header float-right">
        					<div class="page-title">
        						<ol class="breadcrumb text-right">
        							<li><a href="<?= site_url('Dashboard') ?>">Dashboard</a></li>
        							<li class="active">Berita</li>
        						</ol>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>

        <div class="content">
        	<div class="animated fadeIn">
        		<div class="row">

        			<div class="col-md-12">
        				<div class="card">
        					<div class="card-header">
        						<div class="card-title">
        							<a href="#" class="btn btn-sm btn-primary" data-toggle="modal"
        								data-target="#beritaModal">
        								<i class="fa fa-plus"></i> Tambah</a>
        						</div>
        					</div>
        					<div class="card-body">
        						<table id="bootstrap-data-table" class="table table-striped table-bordered">
        							<thead>
        								<tr>
        									<th>Tanggal</th>
        									<th>Judul</th>
        									<th>Keterangan</th>
        									<th>Foto</th>
        									<th>Aksi</th>
        								</tr>
        							</thead>
        							<tbody>
        								<?php
                                            foreach ($berita as $b) {
                                        ?>
        								<tr>
        									<td><?= $b->tanggal ?></td>
        									<td><?= $b->judul ?></td>
        									<td><?= $b->keterangan ?></td>
        									<td><img style="width:100px;height:75px;"
        											src="<?= site_url('');?>upload/berita/<?= $b->foto ?>" alt="Logo">
        									</td>
        									<td>
        										<a href="#" class="btn btn-sm btn-warning" data-toggle="modal"
        											data-target="#modal_editberita<?php echo $b->id_berita;?>"><i
        												class="fa fa-edit"></i></a>
        										<a href="berita/delete/<?= $b->id_berita;?>" class="btn btn-danger"><i
        												class="fa fa-trash"></i></a>
        									</td>
        								</tr>
        								<?php
                                            }
                                        ?>

        							</tbody>
        						</table>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div><!-- .animated -->
        </div><!-- .content -->
        <div class="clearfix"></div>

        <div class="modal fade" id="beritaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        	aria-hidden="true">
        	<div class="modal-dialog" role="document">
        		<div class="modal-content">
        			<div class="modal-header">
        				<h5 class="modal-title" id="exampleModalLabel">Tambah Berita</h5>
        				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
        					<span aria-hidden="true">×</span>
        				</button>
        			</div>
        			<div class="modal-body">
        				<form class="user" action="<?= site_url('Berita/tambah');?>" method="POST"
        					data-parsley-validate="true" enctype="multipart/form-data">

        					<div class="form-group">
        						<label>Judul</label>
        						<input type="text" class="form-control" placeholder="Judul" name="judul"
        							data-parsley-required="true">
        					</div>

        					<div class="form-group">
        						<label>Keterangan</label>
        						<textarea class="form-control" name="keterangan"
        							data-parsley-required="true"></textarea>
        					</div>

        					<div class="form-group">
        						<label>Foto</label>
        						<input type="file" name="image">
        					</div>
        			</div>
        			<div class="modal-footer">
        				<button class="btn btn-primary" type="submit">Tambah</button>
        				</form>
        				<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        			</div>
        		</div>
        	</div>
        </div>


        <!-- Modal Edit -->
        <?php
        foreach($berita as $i){
            $id_berita=$i->id_berita;
            $tanggal=$i->tanggal;
            $keterangan=$i->keterangan;
            $judul=$i->judul;
            $foto=$i->foto;

        ?>
        <div class="modal fade" id="modal_editberita<?php echo $id_berita;?>" tabindex="-1" role="dialog"
        	aria-labelledby="largeModal" aria-hidden="true">
        	<div class="modal-dialog">
        		<div class="modal-content">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        				<h3 class="modal-title" id="myModalLabel">Edit Berita</h3>
        			</div>
        			<form class="user" action="<?= site_url('Berita/update');?>" method="POST"
        				data-parsley-validate="true" enctype="multipart/form-data">
        				<div class="modal-body">
        					<input name="id_berita" value="<?php echo $id_berita;?>" class="form-control" type="text"
        						hidden>
        					<div class="form-group">
        						<label>Judul</label>
        						<input type="text" class="form-control" value="<?php echo $judul;?>" placeholder="Judul"
        							name="judul" data-parsley-required="true">
        					</div>

        					<div class="form-group">
        						<label>Keterangan</label>
        						<textarea class="form-control" name="keterangan"
        							data-parsley-required="true"><?php echo $keterangan;?></textarea>
        					</div>

        					<div class="form-group">
        						<label>Foto</label>
        						<input class="form-control" type="file" name="image">
        						<input class="form-control" type="hidden" name="old_image" value="<?php echo $foto;?>">
        					</div>
        				</div>
        				<div class="modal-footer">
        					<button class="btn btn-primary" type="submit">Update</button>
        					<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        				</div>
        			</form>
        		</div>
        	</div>
        </div>
        <?php }?>
