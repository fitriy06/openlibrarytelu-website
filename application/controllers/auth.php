<?php
	class auth extends CI_Controller
	{
		public function index()
		{
			session_destroy();
			$this->load->view('login');
		}
		function login(){
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$where = array(
				'username' => $username,
				'password' => md5($password)
				);
			$cek = $this->db->get_where("user",$where)->num_rows();
			if($cek > 0){
				$data_session = array(
					'nama' => $username,
					'status' => "login"
					);
	 
				$this->session->set_userdata($data_session);
	 
				redirect(base_url("Dashboard"));
	 
			}else{
				$this->session->set_flashdata('error_msg', 'Username or Password is not match');
				redirect("auth");
			}
		}

		public function logout(){
			$this->session->sess_destroy();
			$this->session->set_flashdata('succses_msg', 'Now you are logout.');
			redirect('auth');
		}

	}
?>