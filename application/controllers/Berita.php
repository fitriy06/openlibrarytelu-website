<?php
    class Berita extends CI_Controller{
        
		function __construct(){
			parent::__construct();
		
			if($this->session->userdata('status') != "login"){
				redirect(base_url("auth"));
			}
            $this->load->model('m_berita');
		}
        function index(){
            $data=[
                    'berita' => $this->m_berita->berita()
            ];
            print_r($this->m_berita->get_id());
            $this->load->view('berita/header');
            $this->load->view('template/navbar');
            $this->load->view('berita/berita',$data);
            $this->load->view('template/footer');
        }

        public function tambah(){
            
            $data = [
                        'judul'         => $this->input->post('judul'),
                        'keterangan'    => $this->input->post('keterangan'),
                        'tanggal'       => date('Y-m-d'),
                        'foto'          => $this->do_upload(),
                        'created_by'    => $this->session->userdata('nama')

                    ];

            if ($this->db->insert('berita',$data)) 
                {
                    $this->session->set_flashdata('succses_msg', 'Data berhasil disimpan.');
                    redirect('berita');
                }
            else
                {
                    $this->session->set_flashdata('error_msg', 'Data gagal disimpan.');
                    redirect('berita');
                    }
            
        }

        public function update(){
            if ($this->input->post('id_berita')) 
            {
                if (!empty($_FILES['image']['name'])) {
                    $file = $this->do_upload();
                }else{
                    $file = $this->input->post('old_image');
                }
                $data = [
                        'judul'         => $this->input->post('judul'),
                        'keterangan'    => $this->input->post('keterangan'),
                        'tanggal'       => date('Y-m-d'),
                        'foto'          => $file, 
                        'created_by'    => $this->session->userdata('nama')
                    ];

                
                $this->db->where('id_berita', $this->input->post('id_berita'));
                if ($this->db->update('berita', $data))
                {       print_r($data);
                        $this->session->set_flashdata('succses_msg', 'Data berhasil diubah.');
                        redirect('berita');
                }
                else
                {
                    $this->session->set_flashdata('error_msg', 'Data bahan gagal perbarui.');
                    redirect('berita');
                }
            }
        }
        public function delete($id){
            $this->delete_img($id);
            $this->db->where('id_berita',$id);
            $this->db->delete('berita');
            redirect('berita');

        }
        public function do_upload(){
            $config['upload_path']          = './upload/berita';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['file_name']            = $this->m_berita->get_id();
            $config['max_size']             = 1024;
            // $config['max_width']            = 1024;
            // $config['max_height']           = 768;
         
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('image')) {
                return $this->upload->data('file_name');
            }else{     
                return 'default.jpg';
            }
        }

        public function delete_img($id){
            $img = $this->m_berita->getById($id);
            if($img['foto'] != "default.jpg"){
                $filename = explode(".", $img['foto'])[0];
                return array_map('unlink', glob(FCPATH."upload/berita/$filename.*"));
            }
        }
    }
?>