<?php
	class m_berita extends CI_Model
	{
		function get_id(){
			$this->db->select('berita.id_berita as kode', FALSE);
	        $this->db->order_by('id_berita','DESC');
	        $this->db->limit(1);

	        $query=$this->db->get('berita');

	        if ($query->num_rows()!=0) 
	        {
	           $data=$query->row();
	           $kode=intval($data->kode)+1;
	        } 
	        else
	        {
	            $kode=1;
	        }
	        return $kode;
		}
		function berita(){
	      $this->db->select('*');
	      $this->db->from('berita');
	      $query = $this->db->get();
	      return $query->result();
		}
		function getById($id){
			$this->db->select('*');
			$this->db->from('berita');
			$this->db->where('id_berita',$id);
			$query = $this->db->get();
			return $query->row_array();

		}
	}
?>